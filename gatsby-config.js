module.exports = {
  siteMetadata: {
    title: "Peymaneh's design studio",
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    `gatsby-plugin-styled-components`,
    'gatsby-plugin-postcss-sass',
  ],
  pathPrefix: `/PeymanehDesignStudio`,
}
